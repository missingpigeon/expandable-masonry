const constants = {
    COLUMN_COUNT: 3,
    breakpoint: 767
}

const debounce = (func, timeout) => {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}

const expandCard = (e) => {
    e.stopPropagation();
    e.target.closest('.card').classList.toggle('expanded');
}

const expandTopCards = () => {
    const columns = document.querySelectorAll('.column');
    for (let i = 0; i < columns.length; i++) {
        const card = columns[i].firstChild;
        if (card) card.classList.add('expanded');
    }
}

const rearrange = () => {
    const width = window.innerWidth;
    let columnCount = 1;
    if (width > constants.breakpoint) columnCount = constants.COLUMN_COUNT;
    const columns = document.querySelectorAll('.column');
    for (let i = 0; i < columns.length; i++) {
        columns[i].style.width = `calc((100% - ${(columnCount - 1) * 5}px + 10px) / ${columnCount})`;
    }
}

const masonry = (elem) => {
    const masonryElement = elem;
    for (let i = 0; i < constants.COLUMN_COUNT; i++) {
        const column = document.createElement('div');
        column.classList.add('column');
        column.id = `column-${(i + 1).toString()}`;
        masonryElement.appendChild(column);
    }
    const cards = document.querySelectorAll('.card');
    const cardsPerColumn = Math.ceil(cards.length / constants.COLUMN_COUNT);
    let currentColumn = 1;
    for (let j = 1; j <= cards.length; j++) {
        const card = cards[j - 1];
        if (j % 2 === 0) card.classList.add('even');
        else card.classList.add('odd');
        const targetColumn = document.querySelector(`#column-${currentColumn.toString()}`);
        targetColumn.appendChild(masonryElement.removeChild(card));
        if (j % cardsPerColumn === 0) currentColumn += 1.0;
    }
    masonryElement.classList.add('masonry');
    rearrange();
    const width = window.innerWidth;
    if (width > constants.breakpoint) expandTopCards();
}

const cardsContainer = document.querySelector('.content');
masonry(cardsContainer);

const cardHeaders = document.querySelectorAll('.card-header');
for (let i = 0; i < cardHeaders.length; i++) {
    cardHeaders[i].addEventListener('click', expandCard);
}
window.addEventListener('resize', debounce(rearrange, 200));